from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)
        # if o is an instance of datetime
        #    return o.isoformat()
        # otherwise
        #    return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            for i in self.properties:
                value = getattr(o, i)
                if i in self.encoders:
                    encoder = self.encoders[i]
                    value = encoder.default(value)
                d[i] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)
        pass

    def get_extra_data(self, o):
        return {}
